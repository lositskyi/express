const { getData, saveData } = require("../repositories/user.repository");

const getUsers = (id = '') => {
  if (id) {
    const user = getData().find(element =>  element.id == id);

    if (user) {
      return user;
    } else {
      console.error('user with this id is not find');
    }

  } else {
    return getData();
  }
};

const getName = (user) => {
  if (user) {
    return user.first_name;
  } else {
    return null;
  }
};

const saveUser = (user) => {
  const users = getData();
  const _id = +users[users.length - 1].id + 1;
  const newUser = user;
  newUser.id = _id;
  users.push(newUser);

  saveData(users);

  return user;
};

const changeUser = (id, data) => {
  const users = getUsers();
  const index = users.findIndex(user => user.id == id);

  for (key in data) {
    users[index][key] = data[key];
  }

  saveData(users);

  return data;
};

const deleteUser = (id) => {
  const users = getUsers();
  const index = users.findIndex(user => user.id == id);

  if (index == -1) {
      return console.error('user with this id is not find');
    }

  const deletedUser = users.splice(index, 1);

  saveData(users);

  return deletedUser;
};

module.exports = {
  getName,
  getUsers,
  saveUser,
  changeUser,
  deleteUser
};