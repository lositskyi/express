var express = require('express');
var router = express.Router();

const { getUsers, saveUser, changeUser, deleteUser } = require("../services/user.service");
const { isAuthorized } = require("../middlewares/auth.middleware.js");

//router to get oll users
router.get('/', function(req, res, next) {
  const users = getUsers();

  if (users) {
	res.send(users);
  } else {
	res.status(400).send(`Some error in get users`);
  }
});

//router to get user by id
router.get('/:id', function(req, res, next) {
  const id = req.params.id;
  const user = getUsers(id);

  if (user) {
 	res.send(user);
  } else {
	res.status(400).send(`Some error in get user`);
  }
});

//router to create new user
router.post('/', function(req, res, next) {
  const user = saveUser(req.body);

  if (user) {
    res.send(user);
  } else {
    res.status(400).send(`Some error in post newUser`);
  }
});

//router to change user by id
router.put('/:id', function(req, res, next) {
  const id = req.params.id;
  const data = changeUser(id, req.body);

  if (data) {
    res.send(data);
  } else {
    res.status(400).send(`Some error in put user data`);
  }
});

//delete user by id 
router.delete('/:id', function(req, res, next) {
  const id = req.params.id;
  const user = deleteUser(id);

  if (user) {
    res.send(user);
  } else {
    res.status(400).send(`Some error in delete user`);
  }
});

module.exports = router;
