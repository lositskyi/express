const fs = require('fs');
const file = './data/users.json';

const getData = () => {
	const rawdata = fs.readFileSync(file);
	const data = JSON.parse(rawdata);

	return data;
}

const saveData = (data) => {
	const json = JSON.stringify(data);

	fs.writeFileSync(file, json);

    console.log(`Data is saved`);
}

module.exports = {
  getData,
  saveData
};